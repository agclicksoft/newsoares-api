# NEW SOARES #

É um software que permite 3 tipos de usuários: Admin, Cliente e Mecânico, manipularem ordem de serviços.

### Para que serve este repositório? ###

* Para manter a api (Back-end) do software.
* 0.0.2
* [Acesse aqui]()

### Como faço para configurar? ###

* Resumo da configuração
* Configuração
* Dependências: [ express, dotenv, cors, bcryptjs ]
* Configuração do banco de dados
* Como executar testes
* Instruções de implantação

### Diretrizes de contribuição ###

* Testes de escrita
* Revisão de código
* Outras orientações

### Com quem eu falo? ###

* Com Juan Cleiton <cleiton.duraes@clicksoft.com.br>
* Ou com o time de desenvolvimento da Agência Digital Clicksoft