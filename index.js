const express = require('express')
const dotenv = require('dotenv')
const bodyParser = require('body-parser')
const cors = require('cors')

//CONFIGURAÇÃO DO AMBIENTE DE EXECUÇÃO
dotenv.config({
    path: process.env.NODE_ENV === 'production' ? '.env.prod' : '.env'
})

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors({
    origin: '*',
    optionsSuccessStatus: 200
}))

//IMPORTANDOS AS ROTAS DA APLICAÇÃO
app.use('/', require('./src/app/routes/index'))

const server = app.listen(process.env.PORT, ()=>{
    console.log('Servidor rodando na porta ' + server.address().port)
    console.log('Environment: ' + process.env.NODE_ENV)
    console.log('Acesse ' + process.env.BASE_URL)
})