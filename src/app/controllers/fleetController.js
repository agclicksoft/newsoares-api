/**
* Arquivo: fleetController.js
* Função: ?
*/

// IMPORTAÇÃO DAS LIBs
const fleetModel = require('../models/fleetModel')
const userModel = require('../models/userModel')

exports.index = async (request, response) => {
  try {
    const { page = 1, limit = 6 } = request.query
    const count = await fleetModel.find().countDocuments()
    const fleets = await fleetModel
    .find()
    .limit(limit)
    .skip( (page - 1) * limit )

    return response.status(200).json({
      header:{
        page: parseInt(page),
        limit,
        count
      },
      data: fleets
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Error ao tentar buscar frotas."
    })
  }
}

exports.create = async (request, response) => {
  try {
    const { client_id, number_serie_patrimony, number_fleet } = request.body
    
    const user = await userModel.findById(client_id)
    if(!user) return response.status(400).json({
      success: false,
      message: "Erro ao tentar cadastrar frota."})
      
    const nsp = parseInt(number_serie_patrimony)
    const nf = parseInt(number_fleet)
    
    const fleet = await fleetModel
      .find({
        $and:[
          {"number_serie_patrimony":{ $eq: nsp }},
          {"number_fleet": {$eq: nf }},
          {"client_id": { $eq: client_id } }
        ]})

    if(fleet[0]) return response.status(400).json({
      success: false,
      fleet: fleet,
      message: "Esta frota já se encontra cadastrada."})

    await fleetModel.create(request.body)
    return response.status(200).json({ 
      success: true,
      fleet: fleet,
      message: "Frota cadastrada com sucesso!"
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar cadastrar frota."
    })
  }
}

exports.find = async (request, response) => {
  try {
    const { id } = request.params
    const fleet = await fleetModel.findById(id)
    return response.status(200).json({
      header:{
        count: 1
      },
      data: fleet
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar buscar frota."
    })
  }
}

exports.search = async (request, response) => {
  try {
    const { id } = request.params
    const user = await userModel.findById(id)

    if(!user) return response
      .status(400)
      .json({
        success: false,
        message: "Não foi possível retornar os dados."
      })
    
    const { value = null, limit = 6 } = request.query

    if(!value){
      return response.status(400).json({
        success: false,
        message: "Não foi possível retornar os dados."
      })
    }
    
    const text = new RegExp(value, 'i')
    const fleets = await fleetModel.find({
      $and:[
        { "client_id": { $eq: id } },
        { $or: [ { "number_fleet": { $in: [text] } } ]}
      ]
    }).limit(limit)

    return response.status(200).json({
      header:{
        limit,
        count: fleets.length
      },
      data: fleets
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar pesquisar frotas."
    })
  }
}

exports.update = async (request, response) => {
  try {
    const { id } = request.params

    const { client_id, number_serie_patrimony, number_fleet } = request.body
    
    const user = await userModel.findById(client_id)
    if(!user) return response.status(400).json({
      success: false,
      message: "Erro ao tentar cadastrar frota."})
      
    const nsp = parseInt(number_serie_patrimony)
    const nf = parseInt(number_fleet)
    
    const fleet = await fleetModel
      .find({
        $and:[
          {"number_serie_patrimony":{ $eq: nsp }},
          {"number_fleet": {$eq: nf }},
          {"client_id": { $eq: client_id } }
        ]})

    if(fleet[0]) return response.status(400).json({
      success: false,
      message: "Esta frota já se encontra cadastrada."})


    await fleetModel.findOneAndUpdate({ _id: id }, request.body, { new: true })
    return response.status(201).json({ 
      success: true, 
      message: "Frota atualizada com sucesso!"
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar atualizar frota."
    })
  }
}

exports.fromUser = async (request, response) => {
  try {
    const { id } = request.params
    const user = await userModel.findById(id)

    if(!user) response.status(400).json({
      success: false,
      message: "Erro ao tentar buscar frota."})
    
    const { page = 1, limit = 6 } = request.query
    const fleets = await fleetModel
    .find()
    .where("client_id")
    .equals(id)
    .limit(limit)
    .skip( (page - 1) * limit )
    
    const count = await fleetModel
    .find()
    .where("client_id")
    .equals(id)
    .countDocuments()

    return response.status(200).json({
      header:{
        page: parseInt(page),
        limit,
        count
      },
      data: fleets
    })

    
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar buscar frotas."
    })
  }
}

exports.allFromUser = async (request, response) => {
  try {
    const { id } = request.params
    const user = await userModel.findById(id)
    
    if(!user) response.status(400).json({
      success: false,
      message: "Erro ao tentar buscar frota."})
      
    const fleets = await fleetModel.find({
      $and:[{
        "client_id": { $eq: id }
      }]
    })

    const count = await fleetModel
    .find()
    .where("client_id")
    .equals(id)
    .countDocuments()
    
    return response.status(200).json({
      header:{
        count
      },
      data: fleets
    })
    } catch (error) {
      return response.status(400).json({
      success: false,
      message: "Erro ao tentar buscar frotas."
    })
  }
}