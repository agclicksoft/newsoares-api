/**
 * Arquivo: logonController.js
 * Função: Permitir acesso dos usuários ao sistema
 */

// IMPORTAÇÃO DAS LIBs
const userModel = require('../models/userModel')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

// IMPORTAÇÃO DAS CONFIGs
const keys = require('../../config/keys.json') 

exports.index = async (request, response) => {
  try {
    const { email, password } = request.body
    const user = await userModel.findOne({ email }).select('+password +type_user')

    if( !user ){
      return response.status(401).json({
        success: false,
        message: "Por favor, informe um acesso válido."
      })
    }

    if ( !await bcrypt.compare(password, user.password) ){
        return response.status(400).json({ 
          success: false,
          message: "Senha Por favor, informe um acesso válido.",
        });
    }

    user.password = undefined;
    const id = user._id
    const token = jwt.sign({ id }, keys.jwt.secret, { expiresIn: keys.jwt.expiration });
    return response.status(200).json({ id, token, type:user.type_user.id });
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Error ao tentar acessar o sistema.",
      error
    })
  }
}
