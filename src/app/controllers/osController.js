/**
* Arquivo: osController.js
* Função: Manipular ações relacionadas a usuários
*/

// IMPORTAÇÃO DAS LIBs
const moment = require('moment')

const osModel = require('../models/osModel')
const userModel = require('../models/userModel')
const fleetModel = require('../models/fleetModel')
const imageModel = require('../models/imageModel')
const infoJson = require('../utils/json/info.json')

exports.all = async (request, response) => {
  try {
    const os = await osModel.find()
    return response.status(200).json(os)
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Error ao tentar buscar OS."
    })
  }
}

exports.find = async (request, response) => {
  try{
    const { id } = request.params
    const { uid = null } = request.query
    let os = null, user = null, header = null
    if(uid){
      user = await userModel.findById(uid)
      
      if(user.type_user.id == infoJson.type.user.client.id){
        os = await osModel
        .find({
          $and:[
            {_id:id},
            {"client.id": uid}
          ]
        })
        
      }else if(user.type_user.id == infoJson.type.user.mechanic.id){
        os = await osModel
        .find({
          $and:[
            {_id:id},
            {allocated:[uid]}
          ]
        })
      }

      if(!os){
        return response.status(400).json({
          success: false,
          message: "Erro ao tentar encontrar O.S."
        })
      }
      os = os[0]
      header = {
        count: 1,
        client: await userModel.findById(os.client.id)
      }
      
    }else{

      os = await osModel
      .findById(id)
      user = await userModel.findById(os.client.id)
      header = {
        count: 1,
        fleet: user.fleet
      }
    }
    const attachments = await imageModel.find().where("os_id").equals(id)
    return response.status(200).json({
      header,
      data: os,
      attachments: attachments[0]
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar encontrar O.S."
    })
  }
}

exports.user = async (request, response) => {
  try{
    const { id } = request.params
    const user = await userModel.findById(id)
    
    const thisMoment = moment().format()
    const lastThirtyDays = moment(thisMoment)
      .subtract(30, 'days').format();

    const managerActivity = {
      count: 0,
      accumulated: 0,
      lastThirtyDays: 0,
      okay: 0,
      opened: 0,
      billed: 0,
      preventive: 0,
      corrective: 0
    }

    if(user.type_user.id == infoJson.type.user.client.id){
      //Cliente
      managerActivity.count = await osModel
      .find({ "client.id": id }).countDocuments()

      managerActivity.accumulated = await osModel
      .find({
        $and:[
          { "client.id": id },
          { created: { $lte: lastThirtyDays } },
          { "status_os.id": { $ne: infoJson.status.os.closed.id } }
        ]}).countDocuments()

      managerActivity.lastThirtyDays = await osModel
      .find({
        $and:[
          { "client.id": id },
          { created: { $gte: lastThirtyDays, $lte: thisMoment } },
          { "status_os.id": { $ne: infoJson.status.os.okay.id } }
        ]}).countDocuments()

      managerActivity.okay = await osModel
      .find({
        $and:[
          { "client.id": id },
          { "status_os.id": { $eq: infoJson.status.os.okay.id } }
        ]}).countDocuments()
      
      managerActivity.opened = await osModel
      .find({
        $and:[
          { "client.id": id },
          { "status_os.id": { $eq: infoJson.status.os.opened.id } }
        ]}).countDocuments()

      managerActivity.billed = await osModel
      .find({
        $and:[
          { "client.id": id },
          { "status_nf.id": { $eq: infoJson.status.nf.billed.id } }
        ]}).countDocuments()

      managerActivity.preventive = await osModel
      .find({
        $and:[
          { "client.id": id },
          { "status_nf.id": { $eq: infoJson.type.service.preventive.id} }
        ]}).countDocuments()
      
      managerActivity.corrective = await osModel
      .find({
        $and:[
          { "client.id": id },
          { "status_nf.id": { $eq: infoJson.type.service.corrective.id} }
        ]}).countDocuments()

    }else if(user.type_user.id == infoJson.type.user.mechanic.id){
      // Mecânico
      managerActivity.count = await osModel
      .find({ allocated:[id] }).countDocuments()

      managerActivity.accumulated = await osModel
      .find({
        $and:[
          { allocated:[id] },
          { created: { $lte: lastThirtyDays } },
          { "status_os.id": { $ne: infoJson.status.os.closed.id } }
        ]}).countDocuments()

      managerActivity.lastThirtyDays = await osModel
      .find({
        $and:[
          { allocated:[id] },
          { created: { $gte: lastThirtyDays, $lte: thisMoment } },
          { "status_os.id": { $ne: infoJson.status.os.okay.id } }
        ]}).countDocuments()

      managerActivity.okay = await osModel
      .find({
        $and:[
          { allocated:[id] },
          { "status_os.id": { $eq: infoJson.status.os.okay.id } }
        ]}).countDocuments()
      
      managerActivity.opened = await osModel
      .find({
        $and:[
          { allocated:[id] },
          { "status_os.id": { $eq: infoJson.status.os.opened.id } }
        ]}).countDocuments()

      managerActivity.billed = await osModel
      .find({
        $and:[
          { allocated:[id] },
          { "status_nf.id": { $eq: infoJson.status.nf.billed.id } }
        ]}).countDocuments()

      managerActivity.preventive = await osModel
      .find({
        $and:[
          { allocated:[id] },
          { "status_nf.id": { $eq: infoJson.type.service.preventive.id} }
        ]}).countDocuments()
      
      managerActivity.corrective = await osModel
      .find({
        $and:[
          { allocated:[id] },
          { "status_nf.id": { $eq: infoJson.type.service.corrective.id} }
        ]}).countDocuments()
    }

    
    return response.status(200).json({
      header: managerActivity,
      data: user
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar encontrar usuário."
    })
  }
}

exports.filter = async (request, response) => {
  try{
    const { id } = request.params
    const user = await userModel.findById(id)

    if(!user) return response
      .status(400)
      .json({
        success: false,
        message: "Não foi possível retornar as O.S."
      })
    
    let resultFilter = null
    const thisMoment = moment().format()
    const lastThirtyDays = moment(thisMoment).subtract(30, 'days').format();
    const {
      opened = null,
      inProgress = null,
      waitingForApproval = null,
      okay = null,
      accumulated = null,
      closed = null,
      preventive = null,
      billed = null,
      corrective = null,
      month = null
    } = request.body

    const currentYear = new Date().getFullYear()
    const start = moment(`${currentYear}-${month}-01`)
    const end = moment(start).endOf('month')
    if(user.type_user.id == infoJson.type.user.client.id){
      // Admin
      let or = []
      if(waitingForApproval){
        let and = [
          { "client.id": { $eq: id }},
          { "status_os.id": { $eq: infoJson.status.os.waitingForApproval.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(closed){
        let and = [
          { "client.id": { $eq: id }},
          { "status_os.id": { $eq: infoJson.status.os.closed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(corrective){ 
        let and = [
          { "client.id": { $eq: id }},
          { "type_service.id": { $eq: infoJson.type.service.corrective.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(preventive){
        let and = [
          { "client.id": { $eq: id }},
          {"type_service.id": { $eq: infoJson.type.service.preventive.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(billed){
        let and = [
          { "client.id": { $eq: id }},
          { "status_nf.id": { $eq: infoJson.status.nf.billed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(accumulated) {
        let and = [
          { "client.id": { $eq: id }},
          { "status_os.id": { $ne: infoJson.status.os.closed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        else and = [...and, { created: { $lte: lastThirtyDays } }]
        or = [...or, { $and: and }]
      }

      if(or.length < 1 && month){
        or = [{ $and:[
          { "client.id": { $eq: id }},
          { created: { $gte: start, $lte: end } }
        ]}]
      }
      resultFilter = await osModel.find({ $or: or })

    }else if(user.type_user.id == infoJson.type.user.mechanic.id){
      // Mecânico
      let or = []

      if(inProgress){
        let and = [
          { allocated:[id] },
          { "status_os.id": { $eq: infoJson.status.os.inProgress.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(closed){
        let and = [
          { allocated:[id] },
          { "status_os.id": { $eq: infoJson.status.os.closed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(corrective){ 
        let and = [
          { allocated:[id] },
          { "type_service.id": { $eq: infoJson.type.service.corrective.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(preventive){
        let and = [
          { allocated:[id] },
          {"type_service.id": { $eq: infoJson.type.service.preventive.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(billed){
        let and = [
          { allocated:[id] },
          { "status_nf.id": { $eq: infoJson.status.nf.billed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(accumulated) {
        let and = [
          { allocated:[id] },
          { "status_os.id": { $ne: infoJson.status.os.closed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        else and = [...and, { created: { $lte: lastThirtyDays } }]
        or = [...or, { $and: and }]
      }

      if(or.length < 1 && month){
        or = [{ $and:[
          { allocated: [id] },
          { created: { $gte: start, $lte: end } }
        ]}]
      }
      resultFilter = await osModel.find({ $or: or })
    }else if(user.type_user.id == infoJson.type.user.admin.id){
      // Admin
      let or = []
      if(okay){
        let and = [
          { "status_os.id": { $eq: infoJson.status.os.okay.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }
      if(opened){
        let and = [
          { "status_os.id": { $eq: infoJson.status.os.opened.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }
      if(inProgress){
        let and = [
          { "status_os.id": { $eq: infoJson.status.os.inProgress.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(closed){
        let and = [
          { "status_os.id": { $eq: infoJson.status.os.closed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(corrective){ 
        let and = [
          { "type_service.id": { $eq: infoJson.type.service.corrective.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(preventive){
        let and = [
          {"type_service.id": { $eq: infoJson.type.service.preventive.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(billed){
        let and = [
          { "status_nf.id": { $eq: infoJson.status.nf.billed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        or = [...or, { $and: and }]
      }

      if(accumulated) {
        let and = [
          { "status_os.id": { $ne: infoJson.status.os.closed.id } }
        ]
        if(month) and = [...and, { created: { $gte: start, $lte: end } } ]
        else and = [...and, { created: { $lte: lastThirtyDays } }]
        or = [...or, { $and: and }]
      }

      if(or.length < 1 && month){
        or = [{ $and:[
          { allocated: [id] },
          { created: { $gte: start, $lte: end } }
        ]}]
      }
      resultFilter = await osModel.find({ $or: or })
    }
    
    return response.status(200).json({
      header:{
        count: resultFilter.length,
        limit: 6
      },
      data: resultFilter
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar retornar as O.S."
    })
  }
}

exports.query  = async (request, response) => {
  try {
    const { id = null } = request.params
    const { page = 1, limit = 6 } = request.query
    
    let count = 0, os = null

    if(!id){
      count = await osModel
      .find()
      .countDocuments()
      
      os = await osModel
      .find()
      .limit(limit)
      .skip( (page - 1) * limit )

    }else{
      const user = await userModel.findById(id)

      if(user.type_user.id == infoJson.type.user.client.id){
        count = await osModel
        .find()
        .where("client.id").equals(id)
        .countDocuments()
        
        os = await osModel
        .find()
        .where("client.id").equals(id)
        .limit(limit)
        .skip( (page - 1) * limit )
      }else if(user.type_user.id == infoJson.type.user.mechanic.id){
        count = await osModel
        .find({
          $and:[
            { allocated:[id] },
            { "status_os.id": { $gt: infoJson.status.os.opened.id }}
          ]})
        .countDocuments()

        os = await osModel
        .find({
          $and:[
            { allocated:[id] },
            { "status_os.id": { $gt: infoJson.status.os.opened.id }}
          ]})
        .limit(limit)
        .skip( (page - 1) * limit )
      }
      
    }
    
    return response.status(200).json({
      header:{
        page: parseInt(page),
        limit,
        count
      },
      data: os
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar buscar O.S."
    })
  }
}

exports.latest  = async (request, response) => {
  try {
    const { id = null } = request.params

    if(!id){
      return response.status(400).json({
        success: false,
        message: "Erro ao tentar buscar as últimas O.S."
      })
    }

    const { page = 1, limit = 6 } = request.query
    
    let os = null
    const user = await userModel.findById(id)

    if(user.type_user.id == infoJson.type.user.client.id){
      
      os = await osModel
      .find()
      .sort({"created":-1})
      .where("client.id").equals(id)
      .limit(limit)

    }else if(user.type_user.id == infoJson.type.user.mechanic.id){

      // os = await osModel
      // .find({
      //   $and:[
      //     { allocated:[id] },
      //     { "status_os.id": { $gt: infoJson.status.os.opened.id }}
      //   ]})
      // .limit(limit)
    }
    
    return response.status(200).json({
      header:{
        limit
      },
      data: os
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar buscar as últimas O.S."
    })
  }
}

exports.create = async (request, response) => {
  try {
    const { id = null} = request.body.fleet
    if(!id) return response.status(400).json({
      success: false,
      message: "Error ao tentar abrir O.S."
    })
    const fleet = await fleetModel.findById(id)
    request.body.fleet.number_fleet = fleet.number_fleet
    request.body.fleet.number_serie_patrimony = fleet.number_serie_patrimony
    request.body.fleet.client_id = fleet.client_id

    const { id:cid } = request.body.client
    if(!cid) return response.status(400).json({
      success: false,
      message: "Error ao tentar abrir O.S."
    })
    const user = await userModel.findById(cid)
    request.body.client = { id: cid, name: user.name }

    await osModel.create(request.body)

    return response.status(200).json({
      success: true,
      message: "Sua OS foi aberta com sucesso!"
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Error ao tentar abrir O.S."
    })
  }
}

exports.update = async (request, response) => {
  try {
    const { id } = request.params
    await osModel.findOneAndUpdate({ _id: id }, request.body, { new: true })
    return response.status(201).json({ 
      success: true, 
      message: "OS atualizado com sucesso!",
      data: await osModel.findById(id)
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar atualizar a O.S."
    })
  }
}

exports.delete = async (request, response) => {
  try {
    const { id } = request.params
    if( !await osModel.findOne({ id }) ){
      return response.status(400).json({ 
        success: false, 
        message: "ID da OS inválida."
      })
    }
    
    await osModel.findOneAndDelete({ _id: id })
    return response.status(204)
  } catch (error) {
    return response.status(400).json({ 
      success: false,
      message: "Erro ao tentar deletar usuário."
    })
  }
}

exports.uploads = async (request, response) => {
  try {
    const { id } = request.params
    console.log("ID OS: ", id)
    const os = await osModel.findById(id)
    if(!os){
      return response.status(400).json({
        success: false,
        message: "Erro ao tentar salvar o anexo da O.S."
      })
    }

    const attachments = await imageModel.find().where("os_id").equals(id)

    if(attachments.length > 0){
      let att = attachments[0]
      request.files.forEach(file => {
        att.files = [...att.files, file]
      })
      await imageModel.findOneAndUpdate({ _id: att._id }, att, { new: true })
    }else{
      await imageModel.create({
        os_id: id,
        files: request.files
      })
    }
    
    return response.status(200).json({
      success: true,
      message: "Tudo ocorreu bem"
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar salvar o anexo da O.S."
    })
  }
}