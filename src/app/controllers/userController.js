/**
* Arquivo: userController.js
* Função: Manipular ações relacionadas a usuários
*/

// IMPORTAÇÃO DAS LIBs
const userModel = require('../models/userModel')
const bcrypt = require('bcryptjs')
const infoJson = require('../utils/json/info.json')

exports.all = async (request, response) => {
  try {
    const users = await userModel
    .find()
    .sort({ name: 1 })
    .where("type_user.id").gt(1)
    return response.status(200).json(users)
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Error ao tentar buscar usuários."
    })
  }
}

exports.find = async (request, response) => {
  try{
    const { id } = request.params
    const user = await userModel.findById(id)
    return response.status(200).json({
      header:{
        count: 1
      },
      data: user
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar encontrar usuário."
    })
  }
}

exports.query = async (request, response) => {
  try {
    const { type, page = 1, limit = 6 } = request.query
    const count = await userModel
    .find()
    .countDocuments()
    .where("type_user.id").equals(type)
    
    const users = await userModel
    .find()
    .sort({ name: 1 })
    .limit(limit)
    .skip( (page - 1) * limit )
    .where("type_user.id").equals(type)
    
    return response.status(200).json({
      header:{
        type_user: parseInt(type),
        page: parseInt(page),
        limit,
        count
      },
      data: users
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar encontrar usuário."
    })
  }
}

exports.search = async (request, response) => {
  try {
    const { type = null, value = null, limit = 6 } = request.query

    if(!value || !type){
      return response.status(400).json({
        success: false,
        message: "Não foi possível retornar os dados."
      })
    }
    
    const text = new RegExp(value, 'i')
    let cond = []
    if(type == infoJson.type.user.client){
      cond = [
        {"name": { $in: [text] } },
        {"email": { $in: [text] } },
        {"cnpj": { $in: [text] } }
      ]
    } else{
      cond = [
        {"name": { $in: [text] } },
        {"email": { $in: [text] } },
        {"cpf": { $in: [text] } }
      ]
    }
    const users = await userModel
      .find({
        $and:[
          {"type_user.id": type},
          { $or: cond }
        ]
      })
      .limit(limit)
    
    return response.status(200).json({
      header:{
        limit,
        count: users.length
      },
      data: users
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar pesquisar um registro."
    })
  }
}

exports.create = async (request, response) => {
  try {
    const { email, cpf = null, cnpj = null } = request.body

    if( await userModel.findOne({ email }).select('email') ){
      return response.status(400).json({
        success: false,
        message: "E-mail já está sendo usado por outro usuário."
      })
    }
    
    if(cpf){
      if( await userModel.findOne({ cpf }).select('cpf') ){
        return response.status(400).json({
          success: false,
          message: "Já possuímos um cadastro com este CPF."
        })
      }
    }
    
    if(cnpj){
      if( await userModel.findOne({ cnpj }).select('cnpj') ){
        return response.status(400).json({
          success: false,
          message: "Já possuímos um cadastro com este CNPJ."
        })
      }
    }

    await userModel.create(request.body)

    return response.status(200).json({
      success: true,
      message: "Cadastro realizado com sucesso!"
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Error ao tentar cadastrar."
    })
  }
}

exports.update = async (request, response) => {
  try {
    const { id } = request.params
    const user = await userModel.findById(id)

    const { email = null, cpf = null, cnpj = null } = request.body
    
    if(!email){
      return response.status(400).json({
        success: false,
        message: "E-mail não informado."
      })
    }
    
    if(user.email != email){
      if( await userModel.findOne({ email }).select('email') ){
        return response.status(400).json({
          success: false,
          message: "E-mail já está sendo usado por outro usuário."
        })
      }
    }
    
    if(user.cpf != cpf){
      if(cpf){
        if( await userModel.findOne({ cpf }).select('cpf') ){
          return response.status(400).json({
            success: false,
            message: "Já possuímos um cadastro com este CPF."
          })
        }
      }
    }
    
    if(user.cnpj != cnpj){
      if(cnpj){
        if( await userModel.findOne({ cnpj }).select('cnpj') ){
          return response.status(400).json({
            success: false,
            message: "Já possuímos um cadastro com este CNPJ."
          })
        }
      }
    }
    
    await userModel.findOneAndUpdate({ _id: id }, request.body, { new: true })
    return response.status(201).json({ 
      success: true, 
      message: "Cadastro atualizado com sucesso!"
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar atualizar o cadastro."
    })
  }
}

exports.delete = async (request, response) => {
  try {
    const { id } = request.params
    if( !await userModel.findById(id) )
    return response.status(400).json({ 
      success: false,
      message: "ID do usuário inválido."
    })
    
    await userModel.findOneAndDelete({ _id: id })
    return response.status(204)
  } catch (error) {
    return response.status(400).json({ 
      success: false,
      message: "Erro ao tentar deletar usuário."
    })
  }
}

exports.resetPassword = async (request, response) => {
  try {
    const { id } = request.params
    const { password = '' } = request.body

    if(!password)
      return response.status(400).json({
        success: false,
        message: "Por favor, digite uma senha válida."
      })
    
    const hash = await bcrypt.hash(request.body.password, 10);
    request.body.password = hash;
    
    await userModel.findOneAndUpdate({ _id: id }, request.body, { new: true })
    return response.status(201).json({ 
      success: true, 
      message: "Senha alterada com sucesso!"
    })
  } catch (error) {
    return response.status(400).json({
      success: false,
      message: "Erro ao tentar alterar senha."
    })
  }
}