/**
 * Arquivo: logonMiddleware.js
 * Função: Validar os casos de erros possíveis, ao receber tentativa de acessar o sistema.
 */

// IMPORTAÇÃO DAS UTILs
const isEmailValidUtils = require('../utils/isEmailValid')
const isPasswordValidUtils = require('../utils/isPasswordValid')

module.exports = (request, response, next) => {
  const { email = '', password = '' } = request.body

  if ( !email ){
    return response.status(400).json({
      success: false,
      message: "E-mail não informado."
    })
  }

  if ( !password ){
    return response.status(400).json({
      success: false,
      message: "Senha não informada."
    })
  }
  
  if( !isEmailValidUtils.index(email) ){
    return response.status(400).json({
      success: false,
      message: "Endereço de e-mail inválido."
    })
  }

  // const isPassValid = isPasswordValidUtils.index(password)
  
  // if( !isPassValid.success ){
  //   return response.status(400).json(isPassValid)
  // }

  next()
}