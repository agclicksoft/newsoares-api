/**
 * Arquivo: registerMiddleware.js
 * Função: Validar os casos de erros possíveis, ao tentar registrar usuário.
 */

//IMPORTAÇÃO DAS LIBs
const { cpf:cpfValidator, cnpj:cnpjValidator } = require('cpf-cnpj-validator');

// IMPORTAÇÃO DAS UTILs
const isEmailValidUtils = require('../utils/isEmailValid')
const isPasswordValidUtils = require('../utils/isPasswordValid')

module.exports = (request, response, next) => {
  
  const { 
    name = '',
    cpf = '',
    cnpj = '',
    email = '',
    contact = null,
    address = null,
    type_user = null,
    password } = request.body

  const { 
    zip_code = '',
    street = '',
    number = null,
    neighborhood = '',
    city = '',
    uf = '' } = address

  const { cell_phone = '' } = contact
  const { id:type_user_id = null } = type_user

  if ( 
    !name ||
    !email ||
    !cell_phone ||
    !zip_code ||
    !street ||
    !number ||
    !neighborhood ||
    !city ||
    !uf ||
    !type_user_id ||
    !password
  ){
    return response.status(400).json({
      success: false,
      message: "Por favor, preencha todos os campos obrigatórios."
    })
  }

  if( !isEmailValidUtils.index(email) ){
    return response.status(400).json({
      success: false,
      message: "Endereço de e-mail inválido."
    })
  }

  const isPassValid = isPasswordValidUtils.index(password)

  if( !isPassValid.success ){
    return response.status(400).json(isPassValid)
  }

  if(type_user_id === 2){
    if(!cnpj){
      return response.status(400).json({
        success: false,
        message: "Nenhum CNPJ informado."
      })
    }

    if( cnpj && !cnpjValidator.isValid(cnpj) ){
      return response.status(400).json({
        success: false,
        message: "Número de CNPJ inválido."
      })
    }
  }

  if(type_user_id === 1 || type_user_id === 3){
    if(!cpf){
      return response.status(400).json({
        success: false,
        message: "Nenhum CPF informado."
      })
    }

    if( !cpfValidator.isValid(cpf) ){
      return response.status(400).json({
        success: false,
        message: "Número de CPF inválido."
      })
    }
  }

  next()
}