/**
 * Arquivo: fleetsModel.js
 * Função: Criar a collection Os no banco de dados
 */

// IMPORTAÇÃO DAS LIBs
const mongoose = require('../../database/mongoose')

const fleetSchema = new mongoose.Schema(
  {
    number_fleet: { 
      type: String,
      require: false
    },
    number_serie_patrimony: {
      type: String,
      require: false
    },
    client_id: {
      type: String,
      require: false
    },
    created: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: 'fleets'
  }
)

const fleetModel = mongoose.model('fleetModel', fleetSchema)

module.exports = fleetModel