/**
 * Arquivo: osModel.js
 * Função: Criar a collection Os no banco de dados
 */

// IMPORTAÇÃO DAS LIBs
const mongoose = require('../../database/mongoose')

const osSchema = new mongoose.Schema(
  {
    fleet: {
      id: {
        type: String,
        require: true
      },
      client_id: { 
        type: String
      },
      number_fleet: { 
        type: String,
        require: true
      },
      number_serie_patrimony: {
        type: String,
        require: true
      }
    },
    model: {
      type: String,
      require: true,
    },
    evaluate_service:{
      id: { 
        type: Number,
        require: true,
      },
      description: { 
        type: String,
        require: true,
      }
    },
    evaluation:{
      mechanicId: {type:String},
      parts_supplies: {type:String},
      mechanic_notes:{type:String},
      created: {type:String}
    },
    horimetro: {
      type: Number,
      require: true,
    },
    type_service: {
      id: { 
        type: Number,
        require: true,
      },
      description: { 
        type: String,
        require: true,
      }
    },
    equipment_situation: {
      id: { 
        type: Number,
        require: true,
      },
      description: { 
        type: String,
        require: true,
      }
    },
    customer_notes: {
      type: String,
      require: true,
    },
    client:{
      type: Object,
      require: true,
    },
    allocated:{
      type: Array,
    },
    estimate: {
      type: Number,
    },
    status_os: {
      id: { 
        type: Number,
        require: true,
      },
      description: { 
        type: String,
        require: true,
      }
    },
    status_nf: {
      id: { 
        type: Number,
        require: true,
      },
      description: { 
        type: String,
        require: true,
      }
    },
    created: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: 'os'
  }
)

// osSchema.pre("save", async function(next) {
//   this.evaluation.files = [];
//   next();
// })

const osModel = mongoose.model('osModel', osSchema)

module.exports = osModel