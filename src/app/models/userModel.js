/**
 * Arquivo: userModel.js
 * Função: Criar a collection User no banco de dados
 */

// IMPORTAÇÃO DAS LIBs
const mongoose = require('../../database/mongoose')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      require: true
    },
    cpf: {
      type: String,
    },
    cnpj: {
      type: String,
    },
    email: {
      type: String,
      require: true,
    },
    contact: {
      telephone: { type: String },
      cell_phone: { 
        type: String,
        require: true,
      }
    },
    address: {
      zip_code:{
        type: String,
        require: true,
      },
      street:{
        type: String,
        require: true,
      },
      number: {
        type: Number,
        require: true,
      },
      complement: { type: String },
      neighborhood: {
        type: String,
        require: true,
      },
      city: {
        type: String,
        require: true,
      },
      uf: {
        type: String,
        require: true,
      }
    },
    type_user: {
        id: { 
          type: String,
          require: true, 
        },
        description: { type: String }
    },
    password: {
        type: String,
        select: false,
        require: true,
    },
    enable: {
        type: Number,
        require: true,
    },
    vehicle_license_plate:{
      type: String,
      require: true,
    },
    created: {
        type: Date,
        default: Date.now,
    },
  },
  {
    collection: 'users'
  }
)

userSchema.pre("save", async function(next) {
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;
  next();
})

const userModel = mongoose.model('userModel', userSchema)

module.exports = userModel