const routes = require('express').Router()

// IMPORTANDO CONTROLLERS
const fleetController = require('../controllers/fleetController')

routes.get('/fleets', fleetController.index)
routes.get('/fleets/:id', fleetController.find)
routes.get('/fleets/search/:id', fleetController.search)
routes.get('/fleets/my/:id', fleetController.fromUser)
routes.get('/fleets/my/:id/all', fleetController.allFromUser)
routes.post('/fleets/new', fleetController.create)
routes.put('/fleets/update/:id', fleetController.update)

module.exports = routes;