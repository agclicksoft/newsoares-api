/**
 * Arquivo: index.js
 * Função: Manter as rotas que dão acesso as funcionalidade da API 
 */
//IMPORTAÇÃO DAS LIBs
const express = require('express')

const routes = express.Router()

//ROTAS
const usersRoutes = require('./usersRoutes')
const fleetsRoutes = require('./fleetsRoutes')
const osRoutes = require('./osRoutes')

routes.use('/users', usersRoutes)
routes.use('/users', fleetsRoutes)
routes.use('/os', osRoutes)
module.exports = routes
