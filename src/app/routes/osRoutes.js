const routes = require('express').Router()

// IMPORTANDO CONTROLLERS
const osController = require('../controllers/osController')

//IMPORTANDO MIDDLEWARES
const multer = require('multer')
const multerConfig = require('../../config/multer')

routes.get('/my/:id', osController.query)
routes.get('/latest/:id', osController.latest)
routes.get('/user/:id', osController.user)
routes.get('/find', osController.query)
routes.get('/find/:id', osController.find)
routes.post('/new', osController.create)
routes.post('/filter/:id', osController.filter)
routes.post('/:id/uploads', multer(multerConfig).array('file', 12), osController.uploads)
routes.put('/update/:id', osController.update)

module.exports = routes;