const routes = require('express').Router()

// IMPORTANDO CONTROLLERS
const userController = require('../controllers/userController')
const logonController = require('../controllers/logonController')
const fleetController = require('../controllers/fleetController')

//IMPORTANDO MIDDLEWARES
const registerMiddleware = require('../middlewares/registerMiddleware')
const logonMiddleware = require('../middlewares/logonMiddleware')

routes.get('/', userController.all)
routes.post('/login', logonMiddleware, logonController.index)
routes.get('/find', userController.query)
routes.get('/find/:id', userController.find)
routes.get('/search', userController.search)
routes.post('/register', registerMiddleware, userController.create)
routes.put('/update/:id', userController.update)
routes.put('/password/:id', userController.resetPassword)
routes.delete('/delete/:id', userController.delete)

module.exports = routes;