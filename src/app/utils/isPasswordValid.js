const regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#])(?:([0-9a-zA-Z$*&@#])(?!\1)){8,}$/;
// console.log(r.test('a12B@cde')); // true
// console.log(r.test('a12B@cce')); // false
// console.log(r.test('a22B@cde')); // false
// console.log(r.test('a12@@cde')); // false
// console.log(r.test('a12B@cCe')); // true

exports.index = password => {
  
  if ( typeof password !== "string" ){
    return {
      success: false,
      message: "A senha precisa ser do tipo texto."
    }
  }

  if ( password.length < 8 ){
    return {
      success: false,
      message: "A senha precisa ter no mínimo 8 caracteres."
    }
  }

  if( !regex.test(password) ){
    return {
      success: false,
      message: "Sua senha precisa conter ao menos 1 número, 1 letra maiúcula, 1 letra minúscula, 1 caractere especial."
    }
  }

  return {
    success: true,
    message: "Senha válida."
  }
}