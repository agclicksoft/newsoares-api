/**
 * Arquivo: mongoose.js
 * Função: Abrir conexão com o MongoDB
 */

// IMPORTAÇÃO DAS LIBs
const mongoose = require('mongoose');
const db = require('../config/db.json')

const database = process.env.NODE_ENV === 'production'
  ? db.production : db.development

mongoose.connect(
  database.mongoDB.connection,
  { 
    useNewUrlParser: true, 
    useUnifiedTopology: true
  }
);

mongoose.Promise = global.Promise;

module.exports = mongoose;